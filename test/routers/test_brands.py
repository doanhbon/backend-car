import os
import shutil
import unittest

from fastapi.testclient import TestClient
from sqlalchemy import select, delete, insert

from src.database import SessionLocal
from src.main import app
from src.models import Brand


class BrandAPITestCases(unittest.TestCase):
    client = TestClient(app)

    def setUp(self):
        session = SessionLocal()
        session.execute(delete(Brand))
        session.commit()
        session.close()
        self.delete_all_static_files()

    def tearDown(self):
        session = SessionLocal()
        session.execute(delete(Brand))
        session.commit()
        session.close()
        self.delete_all_static_files()

    def delete_all_static_files(self):
        folder = 'static'
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    def test_get_all_brands(self):
        session = SessionLocal()
        session.scalars(
            insert(Brand).returning(Brand),
            [
                {"name": "test", "logo_url": "test_url", "status": True},
                {"name": "test2", "logo_url": "test_url", "status": True}
            ],
        ).all()
        session.commit()
        session.close()

        response = self.client.get("/api/v1/brands")

        assert response.status_code == 200
        json = response.json()
        assert json["status"] == "OK"
        assert json["errors"] == []
        assert len(json["data"]) == 2

    def test_add_brand(self):
        with open("test/files/logo.jpg", "rb") as f:
            filebody = f.read()
        files = {"logo": ("logo.jpg", filebody)
                 }
        data = {
            "name": "test",
            "description": "description test",
            "status": "true"
        }
        response = self.client.post("/api/v1/brands",
                                    data=data,
                                    files=files,
                                    headers={"Content-Type": "multipart/form-data;boundary=boundary"}
                                    )
        assert response.status_code == 200
        json = response.json()
        assert json["status"] == "OK"
        assert json["errors"] == []
        assert json["data"]["name"] == data["name"]
        assert json["data"]["description"] == data["description"]
        assert json["data"]["status"] == True
        session = SessionLocal()
        brand = session.scalars(select(Brand).where(Brand.name == data["name"])).one()
        session.close()

        assert brand.name == data['name']
        assert brand.description == data['description']
        assert brand.status == True

    def test_update_brand(self):
        session = SessionLocal()
        brand = session.scalars(
            insert(Brand).returning(Brand),
            [
                {"name": "test2", "logo_url": "test_url", "status": True}
            ],
        ).all()[0]
        brand_id = brand.id
        session.commit()
        session.close()
        with open("test/files/logo.jpg", "rb") as f:
            filebody = f.read()
        files = {"logo": ("logo.jpg", filebody)
                 }
        data = {
            "name": "test update brand",
            "description": "description test update brand",
            "status": "false"
        }
        response = self.client.put(f"/api/v1/brands/{brand_id}",
                                   data=data,
                                   files=files,
                                   headers={"Content-Type": "multipart/form-data;boundary=boundary"}
                                   )
        assert response.status_code == 200
        json = response.json()
        assert json["status"] == "OK"
        assert json["errors"] == []
        assert json["data"]["name"] == data["name"]
        assert json["data"]["description"] == data["description"]
        assert json["data"]["status"] == False
        session = SessionLocal()
        brand = session.scalars(select(Brand).where(Brand.id == brand_id)).one()
        session.close()

        assert brand.name == data['name']
        assert brand.description == data['description']
        assert brand.status == False

    def test_delete_brand(self):
        session = SessionLocal()
        brand = session.scalars(
            insert(Brand).returning(Brand),
            [
                {"name": "test", "logo_url": "localhost/test.png", "status": True}
            ],
        ).all()[0]
        brand_id = brand.id
        session.commit()
        session.close()

        response = self.client.delete(f"/api/v1/brands/{brand_id}")
        assert response.status_code == 200
        json = response.json()
        assert json["status"] == "OK"
        assert json["errors"] == []
        assert json["data"]["name"] == "test"
        assert json["data"]["description"] == None
        assert json["data"]["status"] == True
        assert json["data"]["logo_url"] == "localhost/test.png"

        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.id == brand_id)).all()
        session.close()

        assert len(brands) == 0

    def test_get_a_brand(self):
        session = SessionLocal()
        brand = session.scalars(
            insert(Brand).returning(Brand),
            [
                {"name": "test", "logo_url": "test_url", "status": True, 'description': 'this is a description'},
            ],
        ).all()
        brand_id = brand[0].id
        session.commit()
        session.close()

        response = self.client.get(f"/api/v1/brands/{brand_id}")

        assert response.status_code == 200
        json = response.json()
        assert json["status"] == "OK"
        assert json["errors"] == []
        assert json['data']['name'] == 'test'
        assert json['data']['logo_url'] == 'test_url'
        assert json['data']['status'] == True
        assert json['data']['description'] == 'this is a description'
