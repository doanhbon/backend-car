import unittest

from fastapi.testclient import TestClient

from src.main import app


class MainAPITestCases(unittest.TestCase):
    client = TestClient(app)

    def test_main(self):
        response = self.client.get("")
        assert response.status_code == 200
        assert response.json() == {"message": "Server is running"}
