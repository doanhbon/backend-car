# Backend Car

## Getting started

Run

```bash
docker-compose build
docker-compose up
```

Note: Sometime, web api will run fail because database container hasn't ready yet, so just turn off containers and
run `docker-compose up` again.

To run test:
```bash
pytest
```