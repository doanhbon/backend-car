from src.common import custom_exception


def create_response(data):
    return {
        "status": custom_exception.ValidationStatusEnum.OK.value,
        "errors": [],
        "data": data
    }
