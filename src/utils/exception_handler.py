from fastapi import Request
from fastapi.responses import JSONResponse

from src.common import custom_exception


async def exception_handler(request: Request, exc: Exception):
    if isinstance(exc, custom_exception.CustomBaseError):
        return JSONResponse(
            status_code=exc.status_code,
            content={"status": exc.status.value, "errors": [{"code": exc.code, "message": exc.detail}], "data": []},
        )
    return JSONResponse(
        status_code=500,
        content={"status": custom_exception.ValidationStatusEnum.ERROR.value, "errors": [
            {"code": custom_exception.ValidationCodeEnum.ERR_SERVER_000.name,
             "message": custom_exception.ValidationCodeEnum.ERR_SERVER_000.value}], "data": []},
    )
