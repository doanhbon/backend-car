import datetime
import os

import aiofiles
from fastapi import UploadFile
from sqlalchemy import select, insert, update, delete
from sqlalchemy.orm import Session

from src import configs
from src.models import Brand
from src.schemas.brand import CreateBrandSchema, UpdateBrandSchema, DeleteBrandSchema, GetBrandSchema


def get_brands(db: Session):
    stmt = select(Brand)
    return db.scalars(stmt).all()


async def create_brand(data: CreateBrandSchema, logo: UploadFile, db: Session):
    brands = db.scalars(
        insert(Brand).returning(Brand),
        [
            data
        ],
    )

    new_added_brand = brands.all()[0]
    file_path = f"static/brand_{new_added_brand.id}.{logo.filename.split('.')[-1]}"
    async with aiofiles.open(file_path, 'wb') as out_file:
        content = await logo.read()  # async read
        await out_file.write(content)
    stmt = (
        update(Brand).where(Brand.id == new_added_brand.id).values(
            logo_url=f"http://{configs.SERVER_PUBLIC_HOST}/{file_path}")
    )
    db.execute(stmt)
    return new_added_brand


async def fully_update_brand(data: UpdateBrandSchema, logo: UploadFile, db: Session):
    if logo is not None:
        file_path = f"static/brand_{data.id}.{logo.filename.split('.')[-1]}"
        async with aiofiles.open(file_path, 'wb') as out_file:
            content = await logo.read()  # async read
            await out_file.write(content)
        stmt = (
            update(Brand).where(Brand.id == data.id).values(logo_url=f"http://{configs.SERVER_PUBLIC_HOST}/{file_path}",
                                                            name=data.name,
                                                            description=data.description, status=data.status,
                                                            updated_at=datetime.datetime.now())
        )
        db.execute(stmt)
    else:
        stmt = (
            update(Brand).where(Brand.id == data.id).values(name=data.name,
                                                            description=data.description, status=data.status,
                                                            updated_at=datetime.datetime.now())
        )
        db.execute(stmt)

    stmt = select(Brand).where(Brand.id == data.id)

    return db.scalars(stmt).one()


def delete_brand(data: DeleteBrandSchema, db: Session):
    brand = db.scalars(select(Brand).where(Brand.id == data.id)).one()

    db.execute(delete(Brand).where(Brand.id == data.id))

    logo_path = os.path.join("static", brand.logo_url.split("/")[-1])
    if os.path.exists(logo_path):
        os.remove(logo_path)

    return brand


def get_brand(data: GetBrandSchema, db: Session):
    return db.scalars(select(Brand).where(Brand.id == data.id)).one()
