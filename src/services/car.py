from sqlalchemy import select, and_
from sqlalchemy.orm import Session

from src.models import Car, Variant, Model, Brand
from src.schemas import car as car_schemas


def get_cars(query: car_schemas.SearchCarSchema, db: Session):
    conditions = []
    if query.brand_id is not None:
        conditions.append(Car.variant.has(Variant.model.has(Model.brand_id == query.brand_id)))
    if query.brand_name is not None:
        conditions.append(Car.variant.has(Variant.model.has(Model.brand.has(Brand.name == query.brand_name))))
    if query.status is not None:
        conditions.append(Car.status == query.status)
    if query.from_price is not None:
        conditions.append(Car.price >= query.from_price)
    if query.to_price is not None:
        conditions.append(Car.price <= query.from_price)
    if query.type_id is not None:
        conditions.append(Car.car_type_id == query.type_id)

    s = select(Car).where(and_(*conditions))

    return db.scalars(s).all()
