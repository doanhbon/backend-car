import datetime
import os

import aiofiles
from fastapi import UploadFile
from sqlalchemy import select, insert, update, delete
from sqlalchemy.orm import Session

from src import configs
from src.models import Model
from src.schemas.model import CreateModelSchema, UpdateModelSchema, DeleteModelSchema


def get_models(db: Session):
    stmt = select(Model)
    return db.scalars(stmt).all()


async def create_model(data: CreateModelSchema, logo: UploadFile, db: Session):
    models = db.scalars(
        insert(Model).returning(Model),
        [
            data
        ],
    )

    new_added_model = models.all()[0]
    file_path = f"static/model_{new_added_model.id}.{logo.filename.split('.')[-1]}"
    async with aiofiles.open(file_path, 'wb') as out_file:
        content = await logo.read()  # async read
        await out_file.write(content)
    stmt = (
        update(Model).where(Model.id == new_added_model.id).values(
            image_url=f"http://{configs.SERVER_PUBLIC_HOST}/{file_path}")
    )
    db.execute(stmt)
    return new_added_model


async def fully_update_model(data: UpdateModelSchema, logo: UploadFile, db: Session):
    if logo is not None:
        file_path = f"static/model_{data.id}.{logo.filename.split('.')[-1]}"
        async with aiofiles.open(file_path, 'wb') as out_file:
            content = await logo.read()  # async read
            await out_file.write(content)
        stmt = (
            update(Model).where(Model.id == data.id).values(
                image_url=f"http://{configs.SERVER_PUBLIC_HOST}/{file_path}",
                name=data.name,
                description=data.description, base_price=data.base_price,
                price_poa=data.price_poa,
                with_coe=data.with_coe,
                monthly_installment_price=data.monthly_installment_price,
                updated_at=datetime.datetime.now())
        )
        db.execute(stmt)
    else:
        stmt = (
            update(Model).where(Model.id == data.id).values(name=data.name,
                                                            description=data.description, base_price=data.base_price,
                                                            price_poa=data.price_poa,
                                                            with_coe=data.with_coe,
                                                            monthly_installment_price=data.monthly_installment_price,
                                                            updated_at=datetime.datetime.now())
        )
        db.execute(stmt)

    stmt = select(Model).where(Model.id == data.id)

    return db.scalars(stmt).one()


def delete_model(data: DeleteModelSchema, db: Session):
    model = db.scalars(select(Model).where(Model.id == data.id)).one()

    db.execute(delete(Model).where(Model.id == data.id))

    logo_path = os.path.join("static", model.image_url.split("/")[-1])
    if os.path.exists(logo_path):
        os.remove(logo_path)

    return model


def get_model(data: DeleteModelSchema, db: Session):
    return db.scalars(select(Model).where(Model.id == data.id)).one()
