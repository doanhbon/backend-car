import os

DB_USERNAME = os.environ.get("DB_USERNAME", "postgres")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "postgres")
DB_HOST = os.environ.get("DB_HOST", "localhost")
DB_NAME = os.environ.get("DB_NAME", "postgres")
SERVER_PUBLIC_HOST = os.environ.get("SERVER_PUBLIC_HOST", "localhost:8000")
