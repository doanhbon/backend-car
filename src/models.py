import datetime
import enum
from typing import List

from sqlalchemy import ForeignKey, inspect
from sqlalchemy import String, DateTime, Boolean, Double, Enum
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship


class CarStatus(enum.Enum):
    new_authorised_dealer = 'new_authorised_dealer'
    new_parallel_importer = 'new_parallel_importer'
    used = 'used'


class Base(DeclarativeBase):
    __abstract__ = True
    updated_at: Mapped[DateTime] = mapped_column(DateTime, default=datetime.datetime.utcnow)
    created_at: Mapped[DateTime] = mapped_column(DateTime, default=datetime.datetime.utcnow)


class Brand(Base):
    __tablename__ = "brand"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255), nullable=False, unique=True)
    logo_url: Mapped[str] = mapped_column(String, nullable=True)
    description: Mapped[str] = mapped_column(String(255), nullable=True)
    status: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)
    models: Mapped[List["Model"]] = relationship(
        back_populates="brand", cascade="all,delete"
    )


class Model(Base):
    __tablename__ = "model"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255), nullable=False)
    image_url: Mapped[str] = mapped_column(String, nullable=True)
    description: Mapped[str] = mapped_column(String(255), nullable=True)
    base_price: Mapped[float] = mapped_column(Double, nullable=True)
    price_poa: Mapped[bool] = mapped_column(Boolean, nullable=False)
    with_coe: Mapped[bool] = mapped_column(Boolean, nullable=False)
    monthly_installment_price: Mapped[float] = mapped_column(Double, nullable=True)
    brand_id: Mapped[int] = mapped_column(ForeignKey("brand.id"), nullable=False, index=True)
    brand: Mapped["Brand"] = relationship(back_populates="models")
    variants: Mapped[List["Variant"]] = relationship(
        back_populates="model", cascade="all,delete"
    )


class Variant(Base):
    __tablename__ = "variant"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255), nullable=False)
    image_url: Mapped[str] = mapped_column(String, nullable=True)
    description: Mapped[str] = mapped_column(String(255), nullable=True)
    model_id: Mapped[int] = mapped_column(ForeignKey("model.id"), nullable=False, index=True)
    model: Mapped["Model"] = relationship(back_populates="variants")
    cars: Mapped[List["Car"]] = relationship(
        back_populates="variant", cascade="all,delete"
    )


class CarType(Base):
    __tablename__ = "car_type"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255), nullable=False)
    description: Mapped[str] = mapped_column(String(255), nullable=True)


class Car(Base):
    __tablename__ = "car"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String(255), nullable=False)
    image_url: Mapped[str] = mapped_column(String, nullable=True)
    status: Mapped[str] = mapped_column(Enum(CarStatus), nullable=False)
    description: Mapped[str] = mapped_column(String(255), nullable=True)
    price: Mapped[float] = mapped_column(Double, nullable=False)
    depr_price: Mapped[float] = mapped_column(Double, nullable=True)
    rolling_day: Mapped[DateTime] = mapped_column(DateTime, nullable=True)
    odometer: Mapped[float] = mapped_column(Double, nullable=True)
    car_type_id: Mapped[int] = mapped_column(ForeignKey("car_type.id"), nullable=False, index=True)
    variant_id: Mapped[int] = mapped_column(ForeignKey("variant.id"), nullable=False, index=True)
    variant: Mapped["Variant"] = relationship(back_populates="cars")
