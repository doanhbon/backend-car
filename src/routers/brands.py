from typing import Union

from fastapi import APIRouter, Depends, Form, UploadFile, File
from sqlalchemy.orm import Session

from src.common import decorators
from src.database import get_db
from src.schemas import brand as brand_schemas
from src.schemas import car as car_schemas
from src.services import brand as brand_services
from src.services import car as car_services

router = APIRouter()


@router.get("")
@decorators.custom_response_shape
async def list(db: Session = Depends(get_db)):
    return brand_services.get_brands(db)


@router.post("")
@decorators.custom_response_shape
async def create(name: str = Form(), description: str = Form(None), status: bool = Form(), logo: UploadFile = File(),
                 db: Session = Depends(get_db)):
    return await brand_services.create_brand(
        brand_schemas.CreateBrandSchema(name=name, description=description, status=status),
        logo, db)


@router.put("/{brand_id}")
@decorators.custom_response_shape
async def fully_update(brand_id: int, name: str = Form(), description: str = Form(None), status: bool = Form(),
                       logo: Union[UploadFile, None] = File(None),
                       db: Session = Depends(get_db)):
    return await brand_services.fully_update_brand(
        brand_schemas.UpdateBrandSchema(id=brand_id, name=name, description=description, status=status), logo, db)


@router.delete("/{brand_id}")
@decorators.custom_response_shape
async def destroy(brand_id: int, db: Session = Depends(get_db)):
    return brand_services.delete_brand(brand_schemas.DeleteBrandSchema(id=brand_id), db)


@router.get("/{brand_id}")
@decorators.custom_response_shape
async def retrieve(brand_id: int, db: Session = Depends(get_db)):
    return brand_services.get_brand(brand_schemas.GetBrandSchema(id=brand_id), db)


@router.get("/{brand_id}/cars")
@decorators.custom_response_shape
async def list_cars(brand_id: int, db: Session = Depends(get_db)):
    return car_services.get_cars(car_schemas.SearchCarSchema(brand_id=brand_id), db)
