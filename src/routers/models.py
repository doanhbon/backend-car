from typing import Union

from fastapi import APIRouter, Depends, Form, UploadFile, File
from sqlalchemy.orm import Session

from src.common import decorators
from src.database import get_db
from src.schemas import model as model_schemas
from src.services import model as model_services

router = APIRouter()


@router.get("")
@decorators.custom_response_shape
async def list(db: Session = Depends(get_db)):
    return model_services.get_models(db)


@router.post("")
@decorators.custom_response_shape
async def create(name: str = Form(), description: str = Form(None), base_price: float = Form(None),
                 price_poa: bool = Form(), with_coe: bool = Form(), monthly_installment_price: float = Form(None),
                 brand_id: int = Form(), logo: UploadFile = File(),
                 db: Session = Depends(get_db)):
    return await model_services.create_model(
        model_schemas.CreateModelSchema(name=name, description=description, base_price=base_price, price_poa=price_poa,
                                        with_coe=with_coe, monthly_installment_price=monthly_installment_price,
                                        brand_id=brand_id),
        logo, db)


@router.put("/{model_id}")
@decorators.custom_response_shape
async def fully_update(model_id: int, name: str = Form(), description: str = Form(None), base_price: float = Form(None),
                       price_poa: bool = Form(), with_coe: bool = Form(), monthly_installment_price: float = Form(None),
                       brand_id: int = Form(), logo: Union[UploadFile, None] = File(None),
                       db: Session = Depends(get_db)):
    return await model_services.fully_update_model(
        model_schemas.UpdateModelSchema(id=model_id, name=name, description=description, base_price=base_price,
                                        price_poa=price_poa,
                                        with_coe=with_coe, monthly_installment_price=monthly_installment_price,
                                        brand_id=brand_id), logo, db)


@router.delete("/{model_id}")
@decorators.custom_response_shape
async def destroy(model_id: int, db: Session = Depends(get_db)):
    return model_services.delete_model(model_schemas.DeleteModelSchema(id=model_id), db)


@router.get("/{model_id}")
@decorators.custom_response_shape
async def retrieve(model_id: int, db: Session = Depends(get_db)):
    return model_services.get_model(model_schemas.GetModelSchema(id=model_id), db)
