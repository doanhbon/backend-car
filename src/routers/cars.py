from typing import Union

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.common import decorators
from src.database import get_db
from src.schemas import car as car_schemas
from src.services import car as car_services

router = APIRouter()


@router.get("")
@decorators.custom_response_shape
async def list(brand_id: Union[int, None] = None, brand_name: Union[str, None] = None, status: Union[str, None] = None,
               from_price: Union[float, None] = None, to_price: Union[float, None] = None,
               type_id: Union[int, None] = None,
               db: Session = Depends(get_db)):
    return car_services.get_cars(
        car_schemas.SearchCarSchema(brand_id=brand_id, brand_name=brand_name, status=status, from_price=from_price,
                                    to_price=to_price, type_id=type_id), db)
