from functools import wraps

from src.common import custom_exception


def custom_response_shape(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        result = await func(*args, **kwargs)
        return {
            "status": custom_exception.ValidationStatusEnum.OK.value,
            "errors": [],
            "data": result
        }

    return wrapper
