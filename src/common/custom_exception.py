from enum import Enum


class ValidationStatusEnum(Enum):
    OK = 'OK'
    ERROR = 'ERROR'
    WARNING = 'WARNING'


class ValidationCodeEnum(Enum):
    ERR_SERVER_000 = 'Interal server error.'

    ERR_BRAND_000 = 'Name already exists.'
    ERR_BRAND_001 = 'Brand is not exists.'

    ERR_MODEL_000 = 'Model is not exists.'


class CustomBaseError(Exception):
    def __init__(self, validation_code: ValidationCodeEnum):
        super().__init__()
        self.status = ValidationStatusEnum.ERROR
        self.validation_code = validation_code
        self.detail = validation_code.value
        self.code = validation_code.name
        self.status_code = 400


class BadRequestError(CustomBaseError):
    def __init__(self, validation_code: ValidationCodeEnum):
        super().__init__(validation_code)
        self.status_code = 400


class InternalServerError(CustomBaseError):
    def __init__(self, validation_code: ValidationCodeEnum):
        super().__init__(validation_code)
        self.status_code = 500
