from typing import Union

from pydantic import BaseModel, validator
from sqlalchemy import select

from src.common import custom_exception
from src.database import SessionLocal
from src.models import Model, Brand


class CreateModelSchema(BaseModel):
    name: str
    description: Union[str, None] = None
    base_price: Union[float, None] = None
    price_poa: bool
    with_coe: bool
    monthly_installment_price: Union[float, None] = None
    brand_id: int

    @validator('brand_id')
    def must_exists_brand(cls, v):
        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.id == v)).all()
        session.close()
        if len(brands) == 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_BRAND_001)
        return v


class UpdateModelSchema(CreateModelSchema):
    id: int

    @validator('id')
    def must_exists(cls, v):
        session = SessionLocal()
        models = session.scalars(select(Model).where(Model.id == v)).all()
        session.close()
        if len(models) == 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_MODEL_000)
        return v


class DeleteModelSchema(BaseModel):
    id: int

    @validator('id')
    def must_exists(cls, v):
        session = SessionLocal()
        models = session.scalars(select(Model).where(Model.id == v)).all()
        session.close()
        if len(models) == 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_MODEL_000)
        return v


class GetModelSchema(DeleteModelSchema):
    pass
