from typing import Union

from pydantic import BaseModel


class SearchCarSchema(BaseModel):
    brand_id: Union[int, None] = None
    brand_name: Union[str, None] = None
    status: Union[str, None] = None
    from_price: Union[float, None] = None
    to_price: Union[float, None] = None
    type_id: Union[int, None] = None
