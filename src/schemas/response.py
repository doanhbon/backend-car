from typing import Union, List, Any

from pydantic import BaseModel


class ErrorSchema(BaseModel):
    code: str
    message: str


class ResponseSchema(BaseModel):
    status: str
    errors: List[ErrorSchema] = list
    data: Union[List[Any], dict]
