from typing import Union

from pydantic import BaseModel, validator
from sqlalchemy import select

from src.common import custom_exception
from src.database import SessionLocal
from src.models import Brand


class CreateBrandSchema(BaseModel):
    name: str
    description: Union[str, None] = None
    status: bool = True

    '''
        Check if name is exist already.
    '''

    @validator('name')
    def no_duplicates(cls, v):
        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.name == v)).all()
        session.close()
        if len(brands) > 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_BRAND_000)
        return v


class UpdateBrandSchema(CreateBrandSchema):
    id: int
    name: str
    description: Union[str, None] = None
    status: bool = True

    @validator('id')
    def must_exists(cls, v):
        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.id == v)).all()
        session.close()
        if len(brands) == 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_BRAND_001)
        return v

    @validator('name')
    def no_duplicates(cls, v, values):
        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.name == v, Brand.id != 1)).all()
        session.close()
        if len(brands) > 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_BRAND_000)
        return v


class DeleteBrandSchema(BaseModel):
    id: int

    @validator('id')
    def must_exists(cls, v):
        session = SessionLocal()
        brands = session.scalars(select(Brand).where(Brand.id == v)).all()
        session.close()
        if len(brands) == 0:
            raise custom_exception.BadRequestError(custom_exception.ValidationCodeEnum.ERR_BRAND_001)
        return v


class GetBrandSchema(DeleteBrandSchema):
    pass
