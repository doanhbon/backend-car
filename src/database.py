from sqlalchemy import URL
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src import configs

DATABASE_URL = URL.create(
    "postgresql+psycopg2",
    username=configs.DB_USERNAME,
    password=configs.DB_PASSWORD,  # plain (unescaped) text
    host=configs.DB_HOST,
    database=configs.DB_NAME,
)

engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
        db.commit()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()
