import subprocess

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles

from src.routers import brands, models, cars
from src.utils import exception_handler

app = FastAPI()


@app.exception_handler(Exception)
async def unicorn_exception_handler(request: Request, exc: Exception):
    return await exception_handler.exception_handler(request, exc)


app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
def root():
    return {"message": "Server is running"}


app.include_router(brands.router, prefix="/api/v1/brands",
                   tags=["brands"], )
app.include_router(models.router, prefix="/api/v1/models",
                   tags=["models"], )
app.include_router(cars.router, prefix="/api/v1/cars",
                   tags=["cars"], )

subprocess.run(["alembic", "upgrade", "head"])
